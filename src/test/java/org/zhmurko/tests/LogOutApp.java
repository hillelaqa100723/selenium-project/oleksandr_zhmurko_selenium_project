package org.zhmurko.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.zhmurko.pages.base.BasePage;

import static org.assertj.core.api.Assertions.assertThat;

public class LogOutApp extends BaseTest{
    @BeforeMethod
    public void beforeMethod(){
        openUrl();
    }
    @Test
    public void logOutTest(){
        BasePage basePage = new BasePage();

        basePage.getMainMenu().selectLogout();

        assertThat(basePage.getMainMenu().isRegisterMenuItemVisible())
                .as("Register menu item should be visible")
                .isTrue();
        assertThat(basePage.getMainMenu().isLoginMenuItemVisible())
                .as("Login menu item should be visible")
                .isTrue();

    }
}
