package org.zhmurko.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.zhmurko.pages.base.BasePage;
import org.zhmurko.pages.content.ProductPage;
import org.zhmurko.pages.content.computers.NotebooksPage;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AddItemToCard extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void getProducts() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        new NotebooksPage().clickNotebookByName("Asus N551JK-XO076H Laptop");

        Thread.sleep(3000);

        assertThat(new ProductPage()
                .isProductPageLoaded())
                .as("Product Page is loaded").isTrue();

        int initCartValue = new BasePage().getShoppingCartValue();

        new ProductPage().clickOnAddToCartButton();
        Thread.sleep(2000);

        assertThat(new ProductPage()
                .isSuccessMessageDisplayed())
                .as("Success message is displayed").isTrue();

        int finalCartValue = new BasePage().getShoppingCartValue();
        int expectCartValue = initCartValue + 1;
        assertThat(finalCartValue).as("Cart value increased by 1").isEqualTo(expectCartValue);


    }
}
