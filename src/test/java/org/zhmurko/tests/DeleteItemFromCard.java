package org.zhmurko.tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.zhmurko.driver.WebDriverHolder;
import org.zhmurko.pages.base.BasePage;
import org.zhmurko.pages.base.shopping_cart.ShoppingCartPage;
import org.zhmurko.pages.content.computers.NotebooksPage;
import org.zhmurko.pages.login.LoginPage;
import org.zhmurko.utils.PropertyReader;

import static org.assertj.core.api.ClassBasedNavigableIterableAssert.assertThat;
import static org.assertj.core.api.Assertions.assertThat;

public class DeleteItemFromCard extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void deleteProduct() throws Exception {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        int initialCartValue = BasePage.getShoppingCartValue();

        new NotebooksPage().addToCardNotebook("Asus N551JK-XO076H Laptop");

        Thread.sleep(5000);

        new BasePage().getMainMenu().selectShoppingCart();

        int initNumberOfProducts = new ShoppingCartPage().getNumberOfProducts();

        int newCartValue = new BasePage().getShoppingCartValue();
        assertThat(newCartValue)
                .as("Number of products on Shopping cart should be correct")
                .isEqualTo(initialCartValue + 2);

        new ShoppingCartPage().deleteItemFromCart();
        new ShoppingCartPage().getNumberOfProducts();

        int newNumberOfProducts = new ShoppingCartPage().getNumberOfProducts();
        assertThat(newNumberOfProducts)
                .as("Number of products in Shopping card should be correct")
                .isEqualTo(initNumberOfProducts - 1);
    }


}
