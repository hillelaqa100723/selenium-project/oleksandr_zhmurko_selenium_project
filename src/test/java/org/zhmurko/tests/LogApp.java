package org.zhmurko.tests;

import com.google.j2objc.annotations.Property;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.internal.ReporterConfig;
import org.zhmurko.pages.base.BasePage;
import org.zhmurko.utils.PropertyReader;

import static com.codeborne.selenide.Selenide.open;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class LogApp extends BaseTest {
    @BeforeMethod
    public void beforeMethod(){
      openUrl();
    }

    @Test
    public void loginTest() throws InterruptedException {
     String userEmail = PropertyReader.getInstance().getProperty("defaultUser");
     String userPass = PropertyReader.getInstance().getProperty("userPass");


     BasePage basePage = new BasePage().
             getMainMenu().
             selectLogin().
             login(userEmail, userPass);

     assertThat(basePage.getMainMenu()
             .isLogOutMenuItemVisible())
             .as("Logout menu item should be visible")
             .isTrue();
     assertThat(basePage.getMainMenu()
             .isMyAccountMenuItemVisible())
             .as("MyAccount menu item should be visible")
             .isTrue();



    }
}
