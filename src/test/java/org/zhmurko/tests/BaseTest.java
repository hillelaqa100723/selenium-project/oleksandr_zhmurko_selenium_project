package org.zhmurko.tests;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.zhmurko.driver.WebDriverHolder;
import org.zhmurko.utils.PropertyReader;

public class BaseTest {
    @BeforeSuite
    public void beforeClass() {
        WebDriverHolder.getInstance();
    }

    @AfterSuite(alwaysRun = true)
    public void afterClass() {
        WebDriverHolder.getInstance().quitDriver();
    }

    protected void openUrl(String url) {
        String baseUrl = PropertyReader.getInstance().getProperty("baseUrl");
        if (url.startsWith("/"))
            WebDriverHolder.getInstance().getDriver().get(baseUrl + url);
        else WebDriverHolder.getInstance().getDriver().get(url);
    }

    protected void openUrl() {
        openUrl(PropertyReader.getInstance().getProperty("baseUrl"));
    }

}
