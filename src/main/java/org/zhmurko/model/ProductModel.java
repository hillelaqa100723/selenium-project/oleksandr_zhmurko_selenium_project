package org.zhmurko.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(fluent = true)
public class ProductModel implements Comparable<ProductModel> {
    private String productName;
    private Double price;

    public ProductModel(String name, double price) {
    }


    @Override
    public int compareTo(ProductModel o) {
        int compare = productName.compareTo(o.productName());
        return compare == 0 ? price.compareTo(o.price()) : compare;
    }

    private Double price() {
        return null;
    }

    private String productName() {
        return null;
    }


}
