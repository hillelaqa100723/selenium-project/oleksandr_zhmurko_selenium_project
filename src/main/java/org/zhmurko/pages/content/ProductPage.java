package org.zhmurko.pages.content;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.zhmurko.pages.base.BasePage;

public class ProductPage extends BasePage {
    @FindBy(id = "add-to-cart-button-8")
    public WebElement addToCardButton;

    @FindBy(xpath = "//div[contains(@class,'bar-notification success')]")
    private WebElement successMessage;


    @FindBy(xpath = "//strong[contains(text(),'Related products')]")
    private WebElement pageTitle;


    public boolean isProductPageLoaded() {
        return pageTitle.isDisplayed();
    }

    public void clickOnAddToCartButton() {
        addToCardButton.click();
    }

    public boolean isSuccessMessageDisplayed() {
        return successMessage.isDisplayed();
    }

}
