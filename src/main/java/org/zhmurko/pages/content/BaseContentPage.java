package org.zhmurko.pages.content;

import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.zhmurko.model.ProductModel;
import org.zhmurko.model.enums.SortByDirection;
import org.zhmurko.pages.base.BasePage;

import java.util.LinkedList;
import java.util.List;

import static java.lang.Thread.sleep;

public class BaseContentPage extends BasePage {
    @FindBy(css = ".page-title h1")
    private WebElement contentPageTitle;

    @FindBy(id = "products-orderby")
    private WebElement sortByElement;

    @FindBy(id = "products-pagesize")
    private WebElement displaySizeElement;

    @FindBy(css = ".item-box .details")
    private List<WebElement> productDetails;


    public String getContentPageTitleText() {
        return contentPageTitle.getText();
    }

    public BaseContentPage selectSortBy(SortByDirection sortByDirection) throws InterruptedException {
        new Select(sortByElement)
                .selectByValue(sortByDirection.getValue());
        sleep(1500);
        return new BaseContentPage();
    }

    public BaseContentPage selectDisplayPerPage(int count) {
        new Select(displaySizeElement)
                .selectByValue(String.valueOf(count));
        return new BaseContentPage();
    }

    protected List<ProductModel> getProductAsList() {
        List<ProductModel> list = new LinkedList<>();

        for (WebElement product : productDetails) {
            String name = product.findElement(By.cssSelector("h2>a")).getText();
            String priceAsString = product.findElement(By.cssSelector(".actual-price")).getText();
            double price = Double.parseDouble(
                    priceAsString
                            .substring(1)
                            .replaceAll(",", ""));
            ProductModel productModel = new ProductModel(name, price);
            list.add(productModel);
        }
        return list;
    }
    @SneakyThrows
    protected BaseContentPage addItemToCard(String itemName) throws Exception {
        for (WebElement item : productDetails) {
            if (item.findElement(By.cssSelector(".product-title>a")).getText().equals(itemName)) {
                item.findElement(By.cssSelector(".add-info>.buttons>.product-box-add-to-cart-button")).click();
                return this;
            }
        }
        throw new Exception("We did not found item with name " + itemName);
    }

    @SneakyThrows
    protected BaseContentPage addItemToCompare(String itemName) throws Exception {
        for (WebElement item : productDetails) {
            if (item.findElement(By.cssSelector(".product-title>a")).getText().equals(itemName)) {
                item.findElement(By.cssSelector(".add-info>.buttons>.add-to-compare-list-button")).click();
                return this;
            }
        }
        throw new Exception("We did not found item with name " + itemName);
    }


}
