package org.zhmurko.pages.content.computers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.zhmurko.driver.WebDriverHolder;
import org.zhmurko.model.ProductModel;
import org.zhmurko.pages.content.BaseContentPage;

import java.util.List;

public class NotebooksPage extends BaseContentPage {
    public List<ProductModel> getNotebooks(){
        return getProductAsList();
    }
    public NotebooksPage addNoteBookToCart(String noteBookName) throws Exception {
        addItemToCard(noteBookName);
        return new NotebooksPage();
    }

    public NotebooksPage addNoteBookToCompare(String noteBookName) throws Exception {
        addItemToCompare(noteBookName);
        sleep(2000);
        return new NotebooksPage();
    }

    public NotebooksPage addNoteBooksToCompare(List<String> noteBookNames) throws Exception {
        for (String noteBookName : noteBookNames) {
            addNoteBookToCompare(noteBookName);
        }
        return new NotebooksPage();
    }
    public void clickNotebookByName (String notebookName){
        By notebookLocator = By.linkText("Asus N551JK-XO076H Laptop");
        WebElement notebookElement = findElementByLocator(notebookLocator);
    }
    private WebElement findElementByLocator(By locator){
        return WebDriverHolder.getInstance().getDriver().findElement(locator);
    }
    public NotebooksPage addToCardNotebook(String noteName) throws Exception {
        addItemToCard(noteName);
        return new NotebooksPage();
    }


}
