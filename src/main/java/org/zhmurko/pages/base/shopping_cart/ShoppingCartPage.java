package org.zhmurko.pages.base.shopping_cart;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.zhmurko.driver.WebDriverHolder;
import org.zhmurko.pages.base.BasePage;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCartPage extends BasePage {

    public static double getTotalAmountFromShoppingCart() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        WebElement totalAmountElement = driver.findElement(By.xpath("//td/span[@class='value-summary']/strong"));
        String totalAmountText = totalAmountElement.getText();

        return Double.parseDouble(totalAmountText.replaceAll("[^\\d.]+", ""));
    }


    public void deleteItemFromCart() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        By deleteButtonLocator = By.xpath("//button[@class='remove-btn']");
        java.util.List<WebElement> deleteButtons = driver.findElements(deleteButtonLocator);
        if (!deleteButtons.isEmpty()) {
            deleteButtons.get(0).click();
        } else {
            System.out.println("No items to delete in the cart.");
        }

    }

    public int getNumberOfProducts() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        By productNumberLocator = By.xpath("//td[@class='product']");
        java.util.List<WebElement> productElements = driver.findElements(productNumberLocator);
        return productElements.size();
    }



}
