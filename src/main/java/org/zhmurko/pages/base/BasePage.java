package org.zhmurko.pages.base;

import lombok.SneakyThrows;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.zhmurko.driver.WebDriverHolder;
import org.zhmurko.pages.main_menu.MainMenu;
import org.zhmurko.pages.navigate_menu.NavigateMenu;

public class BasePage {
    @FindBy(id = "customerCurrency")
    private WebElement selectCurrency;

    @FindBy(xpath = "//span[@class='cart-qty']")
    private static WebElement cartQuantity;



    public BasePage() {
        PageFactory.initElements(WebDriverHolder.getInstance().getDriver(), this);
    }

    public MainMenu getMainMenu(){
        return new MainMenu();
    }

    public NavigateMenu getNavigationMenu(){
        return new NavigateMenu();
    }

    public BasePage selectCurrency(Currencies currency){
        new Select(selectCurrency).selectByVisibleText(currency.getValue());
        return new BasePage();
    }

    @SneakyThrows
    public void sleep(long ms) throws InterruptedException {
        Thread.sleep(ms);
    }
    public static int getShoppingCartValue() {
        String cartValueText = cartQuantity.getText();
        String numericPart = cartValueText.replaceAll("\\D", "");

        try {
            return Integer.parseInt(numericPart);
        } catch (NumberFormatException e) {
            return -1;
        }
    }


}
