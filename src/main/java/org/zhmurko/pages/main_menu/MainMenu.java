package org.zhmurko.pages.main_menu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.zhmurko.driver.WebDriverHolder;
import org.zhmurko.pages.base.BasePage;
import org.zhmurko.pages.base.shopping_cart.ShoppingCartPage;
import org.zhmurko.pages.login.LoginPage;
import org.zhmurko.pages.my_account.MyAccountPage;

public class MainMenu {
    private void selectMenuItem(MenuItems menuItem) {
        findMenuItem(menuItem).click();
    }

    public LoginPage selectLogin() {
        selectMenuItem(MenuItems.LOG_IN);
        return new LoginPage();
    }

    public MyAccountPage selectMyAccount() {
        selectMenuItem(MenuItems.MY_ACCOUNT);
        return new MyAccountPage();
    }

    public BasePage selectLogout() {
        selectMenuItem(MenuItems.LOG_OUT);
        return new BasePage();
    }
    public ShoppingCartPage selectShoppingCart() {
        selectMenuItem(MenuItems.SHOPPING_CART);
        return new ShoppingCartPage();
    }


    public Boolean isLogOutMenuItemVisible() {
        return findMenuItem(MenuItems.LOG_OUT)
                .isDisplayed();
    }

    public Boolean isMyAccountMenuItemVisible() {
        return findMenuItem(MenuItems.MY_ACCOUNT)
                .isDisplayed();
    }

    public Boolean isRegisterMenuItemVisible(){
        return findMenuItem(MenuItems.REGISTER).isDisplayed();
    }

    public Boolean isLoginMenuItemVisible(){
        return findMenuItem(MenuItems.LOG_IN).isDisplayed();
    }

    private WebElement findMenuItem(MenuItems menuItem) {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        return driver
                .findElement(By.cssSelector(".ico-%s".formatted(menuItem.getValue())));

    }

}
